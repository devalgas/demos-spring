# Introduction au débogage d'une application Java

This repository has a few templates for README files and some notes about which type of information you could write on them.

## Sommaire

I. Lambda expressions

* Qu'est-ce que lambda expressions ou fonction anonyme ?
* Syntaxe
* Capturing
* Interaction avec les expressions lambda
* Méthodes de Références
* Pratique

II. Interface fonctionnelle

* C'est quoi une functional interface en java ?
* Les différents types des interfaces fonctionnelles  interfaces fonctionnelles standard
* Composition fonctionnelle
* Pratique


III. Streams

* Qu'est-ce qu'un streams ?
* Les différents types de streams
* Opérations streams
* Fonctions Collector
* Regroupement
* Pratique


IV. Conclusion

* Comment poursuivre l'apprentissage
* Questions


## I. Lambda expressions

### I.1 Qu'est-ce que lambda expressions ou fonction anonyme ?

Lambda expressions est une fonctionnalité clé du langage de programmation Java à partir de la version 8. 
Elle permet de simplifier la syntaxe pour définir des interfaces fonctionnelles et d'écrire du code plus concis et lisible.

### I.2 Syntaxe

1. Lambda expression simple

```
(x, y) -> x * y
```

Dans cet exemple on multiplie deux nombres, (x, y) est la liste des paramètres et x * y est l'expression. 
La flèche -> est utilisée pour séparer les paramètres de l'expression.

2. Lambda expression avec une interface fonctionnelle

```
MyFunction multiply = (a, b) -> a * b;
```

Dans cet exemple, multiply est une instance de l'interface MyFunction qui utilise une lambda expression pour implémenter la méthode apply(). 
L'expression a * b est évaluée pour retourner le produit de deux nombres entiers.

3. Lambda expression avec des streams

```
List<String> names = Arrays.asList("Amadou", "Ulrich", "Saahir", "Devalgas");

List<String> filteredNames = names.stream()
.filter(name -> name.startsWith("A"))
.collect(Collectors.toList());
```

Dans cet exemple, nous utilisons une lambda expression name -> name.startsWith("A") pour filtrer les noms qui commencent par la lettre "A" à l'aide de la méthode filter(). 
Les noms filtrés sont ensuite collectés dans une nouvelle liste à l'aide de la méthode collect().


### I.3 Capturing

Une expression lambda peut capturer une variable locale ou un paramètre de la méthode appelante, 
ce qui signifie qu'elle peut référencer une variable définie dans une méthode en dehors de la portée de l'expression lambda elle-même. 
Cela permet de créer des expressions lambda plus flexibles et plus réutilisables.

Un exemple simple qui montre comment une expression lambda peut capturer une variable locale:

```
public void doSomething() {
int x = 5;
Runnable r = () -> System.out.println("x = " + x);
r.run();
}
```

Dans cet exemple, nous déclarons une variable x de type int dans la méthode doSomething(). 
Nous utilisons ensuite une expression lambda pour capturer la valeur de x et l'afficher à la console à l'aide de la méthode System.out.println(). 
La variable x est final, c'est-à-dire qu'elle ne peut pas être modifiée dans l'expression lambda.

Si vous essayez de modifier une variable capturée dans une expression lambda, vous obtiendrez une erreur de compilation.

Voici un exemple qui illustre ce point :

```
public void doSomething() {
    int x = 5;
    Runnable r = () -> {
        x = 10; // erreur de compilation ici
        System.out.println("x = " + x);
    };
    r.run();
}
```

### I.4 Interaction avec les expressions lambda

les expressions lambda en Java peuvent être utilisées dans de nombreux contextes différents pour effectuer des opérations sur des collections, 
des streams, des événements, des threads et des interfaces fonctionnelles.

1. Utilisation avec les collections

Les expressions lambda peuvent être utilisées pour parcourir les éléments d'une collection et effectuer une opération sur chaque élément. 
Par exemple, la méthode forEach() peut être utilisée pour appliquer une expression lambda à chaque élément d'une liste.

```
public void doSomething() {
int x = 5;
Runnable r = () -> {
x = 10; // erreur de compilation ici
System.out.println("x = " + x);
};
r.run();
}
```

Dans cet exemple, nous utilisons une expression lambda pour afficher chaque nom de la liste names à la console.

2. Utilisation avec les interfaces fonctionnelles

Les expressions lambda peuvent être utilisées pour implémenter des interfaces fonctionnelles telles que Runnable, Comparator, Predicate, etc. 
Par exemple, la méthode Comparator.comparing() peut être utilisée pour trier une liste d'objets en fonction d'une propriété spécifique.

```
List<Person> people = Arrays.asList(
    new Person("Alice", 25),
    new Person("Bob", 30),
    new Person("Charlie", 20)
);
Collections.sort(people, Comparator.comparing(Person::getAge));
```

Dans cet exemple, nous utilisons une expression lambda pour implémenter la méthode Person.getAge() 
dans l'interface Comparator afin de trier la liste people en fonction de l'âge de chaque personne.

3. Utilisation avec streams

Les expressions lambda peuvent être utilisées pour filtrer, mapper, trier et réduire les éléments d'un flux. 
Par exemple, la méthode filter() peut être utilisée pour filtrer les éléments d'un flux en fonction d'un prédicat.

```
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
List<Integer> evenNumbers = numbers.stream()
                                    .filter(n -> n % 2 == 0)
                                    .collect(Collectors.toList());
```

Dans cet exemple, nous utilisons une expression lambda pour filtrer les nombres pairs de la liste numbers 
et les stocker dans une nouvelle liste appelée evenNumbers.

### I.5 Méthodes de Références

Au lieu d'écrire une expression lambda pour appeler une méthode, vous pouvez utiliser une méthode de référence pour référencer directement la méthode existante.

Voici les différents types de méthodes de références en Java :

1. Référence de méthode statique
   Utilisez le nom de la classe et le nom de la méthode pour référencer une méthode statique existante.

Par exemple :

```
Function<String, Integer> parseInt = Integer::parseInt;
```

Dans cet exemple, nous utilisons la méthode statique Integer.parseInt() pour convertir une chaîne en un entier.
Nous utilisons la méthode de référence Integer::parseInt pour référencer directement la méthode existante.

2. Référence de méthode d'instance

Utilisez une instance existante et le nom de la méthode pour référencer une méthode d'instance existante.

Par exemple :

```
List<String> names = Arrays.asList("Amadou", "Ulrich", "Saahir", "Devalgas");
names.forEach(System.out::println);
```

Dans cet exemple, nous utilisons la méthode forEach() pour afficher chaque nom de la liste names. 
Nous utilisons la méthode de référence System.out::println pour référencer directement la méthode System.out.println().

3. Référence de constructeur de classe

Utilisez le nom de la classe et le mot-clé new pour référencer un constructeur de classe existant. 

Par exemple :

```
Supplier<List<String>> listSupplier = ArrayList::new;
List<String> names = listSupplier.get();
```

Dans cet exemple, nous utilisons la méthode de référence ArrayList::new pour référencer directement le constructeur de la classe ArrayList. 
Nous utilisons ensuite la méthode get() pour créer une nouvelle instance de la liste.

4. Référence de constructeur de tableau

Utilisez le type de tableau et la taille pour référencer un constructeur de tableau existant. 

Par exemple :

```
Function<Integer, int[]> intArrayCreator = int[]::new;
int[] numbers = intArrayCreator.apply(10);
```

Dans cet exemple, nous utilisons la méthode de référence int[]::new pour référencer directement le constructeur de tableau. 
Nous utilisons ensuite la méthode apply() pour créer un nouveau tableau d'entiers de taille 10.


## II. Interface fonctionnelle

### II.1 C'est quoi une functional interface en java ?

Une Functional Interface en Java est une interface qui ne possède qu'une seule méthode abstraite (non-implémentée).
Depuis Java 8, l'utilisation des interfaces fonctionnelles a été facilitée grâce à l'ajout des expressions lambda et des méthodes de référence.
Java possède plusieurs interfaces fonctionnelles intégrées, telles que Runnable, Callable, Comparator, Consumer, Predicate, etc.
Les interfaces fonctionnelles sont également utilisées dans la bibliothèque de flux (Streams) de Java pour définir des opérations sur des flux de données. 
Les interfaces fonctionnelles peuvent être utilisées pour spécifier des fonctions de transformation, de filtrage, de tri, etc.

### II.2 Les différents types des interfaces fonctionnelles 

1. Les interfaces fonctionnelles standard

Java offre un certain nombre d'interfaces fonctionnelles communes qui sont largement utilisées dans la programmation Java.
Ces interfaces fonctionnelles sont présentes dans le package java.util.function.

* Supplier< T > Cette interface représente une fonction qui ne prend pas d'argument et qui retourne une valeur de type T.
* Consumer< T > Cette interface représente une fonction qui prend un argument de type T et ne retourne pas de valeur.
* Function< T, R > Cette interface représente une fonction qui prend un argument de type T et retourne une valeur de type R.
* Predicate<T> Cette interface représente une fonction qui prend un argument de type T et retourne une valeur booléenne.
* UnaryOperator<T> Cette interface représente une fonction qui prend un argument de type T et retourne une valeur de type T.
* BinaryOperator<T> Cette interface représente une fonction qui prend deux arguments de type T et retourne une valeur de type T.
* BiFunction<T, U, R>Cette interface représente une fonction qui prend deux arguments de type T et U et retourne une valeur de type R.

```
// crée une fonction qui retourne un nombre entier aléatoire entre 0 et 99
Supplier<Integer> randomNumber = () -> (int) (Math.random() * 100); 

// appel de la méthode get() de la fonction pour obtenir un nombre aléatoire
int number = randomNumber.get(); 

System.out.println("Nombre aléatoire : " + number);

List<String> names = Arrays.asList("Amadou", "Ulrich", "Saahir", "Devalgas");

// crée une fonction qui prend une chaîne de caractères en entrée et l'affiche à la console
Consumer<String> printName = name -> System.out.println(name); 

// appel de la méthode forEach() de la liste pour appliquer la fonction à chaque élément de la liste
names.forEach(printName); 

// crée une fonction qui prend une chaîne de caractères en entrée et retourne sa longueur
Function<String, Integer> stringLength = str -> str.length(); 

// appel de la méthode apply() de la fonction pour obtenir la longueur de la chaîne de caractères
int length = stringLength.apply("Hello world"); 

System.out.println("Longueur de la chaîne de caractères : " + length);

// création d'une instance de la fonction Predicate qui vérifie si un nombre est pair
Predicate<Integer> isEven = number -> number % 2 == 0;

// appel de la fonction Predicate pour vérifier si un nombre est pair
boolean result = isEven.test(42);

System.out.println(result);

// création d'une instance de la fonction UnaryOperator qui multiplie un nombre par 2
UnaryOperator<Integer> doubleNumber = number -> number * 2;

// Appel de la fonction UnaryOperator pour multiplier un nombre par 2
Integer result = doubleNumber.apply(42);

System.out.println(result);

// création d'une instance de la fonction BinaryOperator qui calcule la somme de deux nombres
BinaryOperator<Integer> addNumbers = (a, b) -> a + b;

// Appel de la fonction BinaryOperator pour calculer la somme de deux nombres
Integer result = addNumbers.apply(40, 2);

System.out.println(result);

// création d'une instance de la fonction BiFunction qui concatène deux Strings
BiFunction<String, String, String> concatenateStrings = (str1, str2) -> str1 + str2;

// appel de la fonction BiFunction pour concaténer deux Strings
String result = concatenateStrings.apply("Hello, World!", " How are you?");

System.out.println(result);
```


2. Les Interfaces fonctionnelles spécialisées

Il est également possible de composer des fonctions avec des interfaces fonctionnelles spécialisées pour différents types de données,
qu'ils sont fournies par le package java.util.function.

* IntFunction< R > Cette interface représente une fonction qui prend un int en entrée et retourne un objet de type R.
* LongFunction< R > Cette interface représente une fonction qui prend un long en entrée et retourne un objet de type R.
* DoubleFunction< R > Cette interface représente une fonction qui prend un double en entrée et retourne un objet de type R.
* IntUnaryOperator Cette interface représente une fonction qui prend un int en entrée et retourne un int.
* LongUnaryOperator Cette interface représente une fonction qui prend un long en entrée et retourne un long.
* DoubleUnaryOperator Cette interface représente une fonction qui prend un double en entrée et retourne un double.
* IntBinaryOperator Cette interface représente une fonction qui prend deux int en entrée et retourne un int.
* LongBinaryOperator Cette interface représente une fonction qui prend deux long en entrée et retourne un long.
* DoubleBinaryOperator Cette interface représente une fonction qui prend deux double en entrée et retourne un double.

```
// déclaration d'une fonction IntFunction<R> qui prend un entier en entrée et renvoie une chaîne de caractères en sortie
IntFunction<String> convertIntToString = x -> String.valueOf(x);

// utilisation de la fonction IntFunction<R> pour convertir un entier en chaîne de caractères
String str = convertIntToString.apply(42);

// déclaration d'une fonction LongFunction<R> qui prend un long en entrée et renvoie une chaîne de caractères en sortie
LongFunction<String> convertLongToString = x -> String.valueOf(x);

// utilisation de la fonction LongFunction<R> pour convertir un long en chaîne de caractères
String str = convertLongToString.apply(123456789L);

// déclaration d'une fonction DoubleFunction<R> qui prend un double en entrée et renvoie une chaîne de caractères en sortie
DoubleFunction<String> convertDoubleToString = x -> String.format("%.2f", x);

// utilisation de la fonction DoubleFunction<R> pour convertir un double en chaîne de caractères
String str = convertDoubleToString.apply(1234.56789);

// déclaration d'une fonction IntUnaryOperator qui prend un entier en entrée et renvoie son carré en sortie
IntUnaryOperator square = x -> x * x;

// utilisation de la fonction IntUnaryOperator pour calculer le carré d'un entier
int result = square.applyAsInt(5);

// déclaration d'une fonction LongUnaryOperator qui prend un long en entrée et renvoie son carré en sortie
LongUnaryOperator square = x -> x * x;

// utilisation de la fonction LongUnaryOperator pour calculer le carré d'un long
long result = square.applyAsLong(5L);

// déclaration d'une fonction DoubleUnaryOperator qui prend un double en entrée et renvoie sa valeur absolue en sortie
DoubleUnaryOperator absolute = x -> Math.abs(x);

// Utilisation de la fonction DoubleUnaryOperator pour calculer la valeur absolue d'un double
double result = absolute.applyAsDouble(-3.14);

// déclaration d'une fonction IntBinaryOperator qui prend deux entiers en entrée et renvoie leur somme en sortie
IntBinaryOperator sum = (x, y) -> x + y;

// utilisation de la fonction IntBinaryOperator pour calculer la somme de deux entiers
int result = sum.applyAsInt(3, 5);

// déclaration d'une fonction LongBinaryOperator qui prend deux long en entrée et renvoie leur somme en sortie
LongBinaryOperator addition = (x, y) -> x + y;

// Utilisation de la fonction LongBinaryOperator pour calculer la somme de deux long
long result = addition.applyAsLong(10L, 20L);

// déclaration d'une fonction DoubleBinaryOperator qui prend deux double en entrée et renvoie leur produit en sortie
DoubleBinaryOperator multiplication = (x, y) -> x * y;

// utilisation de la fonction DoubleBinaryOperator pour calculer le produit de deux double
double result = multiplication.applyAsDouble(2.5, 3.0);
```

### II.3 Composition fonctionnelle

La composition fonctionnelle est un concept important en programmation fonctionnelle qui consiste à combiner des fonctions pour en créer de nouvelles,
ce qui facilite la réutilisation et la modularité du code. 
Les interfaces fonctionnelles en Java fournissent les outils nécessaires pour composer des fonctions de manière élégante et expressive en utilisant les méthodes andThen et compose
En Java, les interfaces fonctionnelles sont des éléments clés de la composition fonctionnelle.

1. La méthode compose

Elle est une méthode fournie par l'interface fonctionnelle Function en Java. 
Permet de composer deux fonctions en créant une nouvelle fonction qui applique la première fonction sur l'entrée, puis applique la seconde fonction sur le résultat.

```
// la fonction addFive ajoute 5 à un entier
Function<Integer, Integer> addFive = x -> x + 5;

// la fonction multiplyByTwo multiplie un entier par 2
Function<Integer, Integer> multiplyByTwo = x -> x * 2;

// La fonction multiplyByTwoAndAddFive multiplie l'entier d'entrée par 2, puis ajoute 5 au résultat
Function<Integer, Integer> multiplyByTwoAndAddFive = addFive.compose(multiplyByTwo);
```

2. La méthode andThen

Elle est une méthode fournie par l'interface fonctionnelle Function en Java. 
Permet de composer deux fonctions en créant une nouvelle fonction qui applique la première fonction sur l'entrée, 
puis applique la seconde fonction sur le résultat de la première.


```
// la fonction square calcule le carré d'un entier
Function<Integer, Integer> square = x -> x * x;


// la fonction toString crée une chaîne de caractères pour afficher ce carré
Function<Integer, String> toString = x -> "Le carré de " + x + " est " + x * x;

// la fonction squareToStringcalcule d'abord le carré de l'entrée, puis crée la chaîne de caractères correspondante
Function<Integer, String> squareToString = square.andThen(toString);

// utilisation de la fonction squareToStringcalcule
String result = squareToString.apply(5);
```

### II.4 Pratique

## III. Streams

### III.1 Qu'est-ce qu'un streams ?

En Java, un stream est une séquence d'éléments qui peut être traitée de manière séquentielle ou parallèle. 
Il s'agit d'un flux de données qui peut être manipulé en utilisant des opérations de traitement de données telles que le filtrage, le tri, le mappage et la réduction.

Les streams permettent de traiter des collections de manière plus efficace et plus expressive que les boucles traditionnelles. 
Ils offrent également la possibilité de traiter des données en parallèle, ce qui peut améliorer les performances lors du traitement de grandes quantités de données.

Les streams sont définis dans le package java.util.stream et peuvent être créés à partir de diverses sources de données telles que des collections, des tableaux, des fichiers ou même des sockets

### III.2 Les différents types de streams

En Java, il existe différents types de streams qui peuvent être utilisés pour effectuer des opérations sur les collections de données.

1. Stream 

C'est le type de base de streams en Java, qui est utilisé pour les opérations séquentielles sur une collection de données. 
Il peut être créé à partir d'une collection ou d'un tableau.

```
// création d'une liste d'entiers
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

// création d'un stream à partir de la liste
List<Integer> evenNumbers = numbers.stream().filter(n -> n % 2 == 0)
                                            .collect(Collectors.toList());
```

2. ParallelStream 

Ce type de stream est similaire au Stream, sauf qu'il est utilisé pour les opérations parallèles sur les données. 
Il peut améliorer les performances lors du traitement de grandes quantités de données.

```
// création d'une liste de nombres entiers et que nous voulions les trier en ordre décroissant à l'aide d'un ParallelStream
List<Integer> numbers = Arrays.asList(1, 5, 3, 7, 2, 8, 4, 9, 6);

// la méthode parallelStream() pour convertir notre liste en un ParallelStream. 
// ensuite, nous avons utilisé la méthode sorted() pour trier les nombres en ordre décroissant à l'aide d'un comparateur en utilisant la méthode Comparator.reverseOrder(). 
// enfin, nous avons collecté les éléments triés dans une nouvelle liste à l'aide de la méthode toList() de Collectors.
List<Integer> sortedNumbers = numbers.parallelStream()
                                      .sorted(Comparator.reverseOrder())
                                      .collect(Collectors.toList());
```

3. IntStream, LongStream, DoubleStream

Ce sont des streams spécialisés pour les types primitifs int, long et double, respectivement. 
Ils peuvent être utilisés pour effectuer des opérations spécifiques à ces types de données, telles que la somme ou la moyenne.

```
int[] intArray = {1, 2, 3, 4, 5};
long[] longArray = {10L, 20L, 30L, 40L, 50L};
double[] doubleArray = {0.5, 1.0, 1.5, 2.0, 2.5};

// somme des éléments du tableau d'entiers
int sumInt = IntStream.of(intArray).sum();

// somme des éléments du tableau de longs
long sumLong = LongStream.of(longArray).sum();

// somme des éléments du tableau de doubles
double sumDouble = DoubleStream.of(doubleArray).sum();

```

4. BaseStream 

C'est une interface de base pour tous les types de streams en Java. 
Elle définit les méthodes communes pour la plupart des opérations de streams, telles que filter et forEach.

```
// création un de nombres entiers de 1 à 9 et filtre les entiers pairs avec filter et et affiche chaque entier restant avec forEach
IntStream.range(1, 10)
         .filter(n -> n % 2 == 0)
         .forEach(System.out::println);
```

### III.3 Opérations streams

Il existe deux types d'opérations sur les streams en Java : les opérations intermédiaires et les opérations finales.

1. Les opérations intermédiaires

Les opérations intermédiaires sont des opérations qui sont appliquées au stream pour le modifier ou le transformer.

* filter(Predicate<T> predicate) : filtre les éléments du stream en fonction d'un prédicat
* map(Function<T, R> mapper) : transforme chaque élément du stream en appliquant une fonction
* flatMap(Function<T, Stream<R>> mapper) : transforme chaque élément du stream en appliquant une fonction qui renvoie un stream, puis combine tous les streams résultants en un seul
* distinct() : renvoie un nouveau stream en supprimant les éléments en double
* sorted() : renvoie un nouveau stream en triant les éléments
* limit(long maxSize) : renvoie un nouveau stream en limitant le nombre d'éléments à un maximum spécifié
* skip(long n) : renvoie un nouveau stream en sautant les n premiers éléments

```
// création d'une liste de nombres
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

// filtrage des nombres impairs
List<Integer> oddNumbers = numbers.stream()
                                  .filter(n -> n % 2 != 0)
                                  .toList();

// transformation des nombres pairs en leur carré
List<Integer> evenSquares = numbers.stream()
                                   .filter(n -> n % 2 == 0)
                                   .map(n -> n * n)
                                   .toList();

// concaténation de deux listes
List<String> firstList = Arrays.asList("foo", "bar");
List<String> secondList = Arrays.asList("baz", "qux");
List<String> combinedList = firstList.stream()
                                     .flatMap(s1 -> secondList.stream().map(s2 -> s1 + s2))
                                     .toList();
// Élimination des doublons
List<Integer> duplicateNumbers = Arrays.asList(1, 2, 3, 1, 2, 3, 4, 5);
List<Integer> distinctNumbers = duplicateNumbers.stream()
                                                .distinct()
                                                .toList();

// Tri des nombres par ordre décroissant
List<Integer> unsortedNumbers = Arrays.asList(3, 6, 1, 8, 2, 5);
List<Integer> sortedNumbers = unsortedNumbers.stream()
                                             .sorted((n1, n2) -> n2 - n1)
                                             .toList();

// Limitation du nombre de résultats
List<Integer> limitedNumbers = numbers.stream()
                                      .limit(5)
                                      .toList();

// ignorer les premiers résultats
List<Integer> skippedNumbers = numbers.stream()
                                      .skip(7)
                                      .toList();
```

   
2. Les opérations finales

Les opérations finales sont des opérations qui consomment le stream et produisent un résultat ou un effet de bord. 
Une fois qu'une opération finale est appliquée à un stream, le stream ne peut plus être utilisé.

* forEach(Consumer<T> action) : applique une action à chaque élément du stream
* toArray() : renvoie un tableau contenant tous les éléments du stream
* reduce(BinaryOperator<T> accumulator) : combine tous les éléments du stream en utilisant un accumulateur spécifié
* collect(Collector<T, A, R> collector) : collecte les éléments du stream dans une structure de données spécifiée
* count() : renvoie le nombre d'éléments dans le stream
* anyMatch(Predicate<T> predicate) : renvoie true si au moins un élément du stream correspond au prédicat spécifié, sinon false
* allMatch(Predicate<T> predicate) : renvoie true si tous les éléments du stream correspondent au prédicat spécifié, sinon false
* noneMatch(Predicate<T> predicate) : renvoie true si aucun élément du stream ne correspond au prédicat spécifié, sinon false
* findFirst() : renvoie le premier élément du stream
* findAny() : renvoie n'importe quel élément du stream (utile pour les streams parallèles)

```
// création d'une liste de strings
List<String> fruits = Arrays.asList("pomme", "banane", "cerise", "pomme", "orange");

// utilisation de la méthode forEach pour afficher chaque élément du stream
fruits.stream().forEach(System.out::println);

// utilisation de la méthode toArray pour convertir le stream en un tableau
String[] fruitsArray = fruits.stream().toArray(String[]::new);

// utilisation de la méthode reduce pour obtenir la concaténation de tous les éléments du stream
String concatenated = fruits.stream().reduce("", (s1, s2) -> s1 + s2);

// Utilisation de la méthode collect pour obtenir une liste des éléments distincts du stream
List<String> distinctFruits = fruits.stream().distinct().collect(Collectors.toList());

// utilisation de la méthode count pour obtenir le nombre d'éléments dans le stream
long count = fruits.stream().count();

// utilisation de la méthode limit pour limiter le nombre d'éléments dans le stream      
List<String> limitedFruits = fruits.stream().limit(3).collect(Collectors.toList());


// utilisation de la méthode allMatch pour vérifier si tous les éléments du stream commencent par "p"
boolean allStartWithP = fruits.stream().allMatch(s -> s.startsWith("p"));
       
// utilisation de la méthode noneMatch pour vérifier si aucun élément du stream ne contient "kiwi"
boolean noneContainsKiwi = fruits.stream().noneMatch(s -> s.contains("kiwi"));

// utilisation de la méthode findFirst pour obtenir le premier élément du stream
String firstFruit = fruits.stream().findFirst().orElse("");


// utilisation de la méthode findAny pour obtenir n'importe quel élément du stream
String anyFruit = fruits.stream().findAny().orElse("");
```

### III.4 Fonctions Collector

les fonctions Collector dans les Streams Java sont utilisées pour collecter les éléments d'un flux 
et les stocker dans une collection ou les traiter d'une manière quelconque.

* toList() : retourne une liste des éléments du flux.
* toSet() : retourne un ensemble des éléments du flux.
* toMap() : retourne une carte des éléments du flux, où chaque élément est associé à une clé calculée à l'aide d'une fonction de clé spécifiée.
* joining() : concatène tous les éléments du flux en une chaîne de caractères.
* counting() : retourne le nombre d'éléments dans le flux.
* averagingInt(), averagingLong(), averagingDouble() : retourne la moyenne des éléments du flux en fonction de leur type (entier, long, double).
* summingInt(), summingLong(), summingDouble() : retourne la somme des éléments du flux en fonction de leur type (entier, long, double).
* maxBy() : retourne l'élément maximum du flux en fonction de l'ordre naturel ou d'un comparateur spécifié.
* minBy() : retourne l'élément minimum du flux en fonction de l'ordre naturel ou d'un comparateur spécifié.

```
// création d'une liste de personnes
List<Person> persons = Arrays.asList(
                new Person("Alice", 25),
                new Person("Bob", 30),
                new Person("Charlie", 35),
                new Person("Dave", 40),
                new Person("Eve", 45)
        );

// utilisation de la fonction toList pour créer une liste à partir du stream
List<String> names = persons.stream()
                .map(Person::getName)
                .collect(Collectors.toList());

// utilisation de la fonction toSet pour créer un ensemble à partir du stream
Set<Integer> ages = persons.stream()
                .map(Person::getAge)
                .collect(Collectors.toSet());

// utilisation de la fonction toMap pour créer une carte à partir du stream
Map<String, Integer> nameAgeMap = persons.stream()
                .collect(Collectors.toMap(Person::getName, Person::getAge));

// utilisation de la fonction joining pour joindre les noms dans une chaîne
String nameString = persons.stream()
                .map(Person::getName)
                .collect(Collectors.joining(", "));

// utilisation de la fonction counting pour compter le nombre de personnes dans le stream
long count = persons.stream()
                .collect(Collectors.counting());

// utilisation de la fonction averagingInt pour calculer la moyenne des âges
        double averageAge = persons.stream()
                .collect(Collectors.averagingInt(Person::getAge));

// utilisation de la fonction summingInt pour calculer la somme des âges
int totalAge = persons.stream()
                .collect(Collectors.summingInt(Person::getAge));

// utilisation de la fonction maxBy pour trouver la personne la plus âgée
Optional<Person> oldestPerson = persons.stream()
                .collect(Collectors.maxBy(Comparator.comparing(Person::getAge)));

// utilisation de la fonction minBy pour trouver la personne la plus jeune
Optional<Person> youngestPerson = persons.stream()
                .collect(Collectors.minBy(Comparator.comparing(Person::getAge)));
```

```
class Person {
    private String name;
    private int age;
    private String city;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + ", "', city=" + city + "}";
    }
}
```

### III.5 Regroupement

1. Grouping 

Permet de regrouper les éléments d'un flux en fonction d'un ou plusieurs critères spécifiques.
Cette opération est courante lors du traitement de données volumineuses,
car elle permet de réduire la taille des données et de faciliter leur analyse.
En Java, cette opération est réalisée en utilisant la méthode groupingBy() du collecteur Collectors.

```
List<Person> persons = Arrays.asList(
                new Person("John", 30, "New York"),
                new Person("Jane", 25, "Boston"),
                new Person("Bob", 40, "Boston"),
                new Person("Alice", 35, "New York"),
                new Person("Joe", 45, "Chicago")
        );
        
// la méthode groupingBy() pour grouper les personnes par ville. 
// le collecteur Collectors.counting() pour compter le nombre de personnes dans chaque ville. 
// la méthode groupingBy() renvoie un objet Map<String, Long> où les clés sont les noms des villes et les valeurs sont le nombre de personnes dans chaque ville
Map<String, Long> countByCity = persons.stream()
                                        .collect(Collectors.groupingBy(Person::getCity, Collectors.counting()));
}
```
2. Partitioning

La partition est une opération spéciale de regroupement de stream qui sépare les éléments en deux groupes en fonction d'une condition booléenne. 
La méthode Collectors.partitioningBy() peut être utilisée pour partitionner un stream en deux groupes.

```
// créé une liste de nombres 
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

// partitionné cette liste en deux groupes : les nombres pairs et les nombres impairs
Map<Boolean, List<Integer>> partitionedNumbers = numbers.stream().collect(Collectors.partitioningBy(n -> n % 2 == 0));

// output: [2, 4, 6, 8]
List<Integer> evenNumbers = partitionedNumbers.get(true);
// output: [1, 3, 5, 7, 9]
List<Integer> oddNumbers = partitionedNumbers.get(false);


}
```

## IV. Conclusion

### IV.1 Comment poursuivre l'apprentissage
* La documentation officielle de Java : la documentation officielle de Java contient des informations détaillées sur les Lambda expressions et les streams en Java, ainsi que des exemples de code.
* Les tutoriels en ligne : il existe de nombreux tutoriels en ligne gratuits pour apprendre les Lambda expressions et les streams en Java, tels que ceux proposés par Oracle, Baeldung ou JavaTpoint.
* Les livres : il existe également de nombreux livres sur Java qui couvrent en profondeur les Lambda expressions et les streams en Java, tels que "Java 8 in Action" de Raoul-Gabriel Urma, Mario Fusco et Alan Mycroft, ou "Functional Programming in Java" de Venkat Subramaniam.
Les cours en ligne : il existe de nombreux cours en ligne payants pour apprendre les Lambda expressions et les streams en Java, tels que ceux proposés par Udemy, Coursera ou edX.

### IV.2 Questions

* Qu'est-ce qu'une expression lambda en Java ?
* Quelle est la différence entre une méthode de référence et une expression lambda en Java 
* Qu'est-ce qu'un stream en Java ?
* Quelle est la différence entre un stream séquentiel et un stream parallèle en Java ?
* Comment peut-on créer un stream à partir d'une collection ou d'un tableau en Java ?
* Quelles sont les opérations courantes que l'on peut effectuer sur un stream en Java ?
* Qu'est-ce qu'une interface fonctionnelle en Java ?
* Quels sont les types d'interfaces fonctionnelles couramment utilisées en Java ?
* Comment peut-on composer des fonctions en Java ?
* Comment peut-on déboguer des expressions lambda et des streams en Java ?